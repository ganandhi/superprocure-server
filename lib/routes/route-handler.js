const projectConfig = require("../../config/config.json")
const ObjectId = require('mongodb').ObjectId;   
const Boom = require('@hapi/boom');
const loginUser = async (req) => {
    let response = {}
    try {
    const loginInfo = req.payload;
    let conditions = {
        userid: loginInfo.userId
    }
    const userInfo = await req.mongo.db.collection('spusers').findOne(conditions);
   
    
    if(loginInfo.password === userInfo.password) {
       
    response = {
        BranchId :userInfo.BranchId
      }
      return response;
    }
    else {
        return Boom.unauthorized("Invalid password");
    }

    
}
catch(err) {
   return  Boom.unauthorized("Invalid Credentials");
}
}
const getBranchMsgByBranchId = async (req) => {

    try {
    

    let conditions = {
        BranchId : req.params.branchId
    }
    
    const branchMsgInfo = await req.mongo.db.collection('spbranchMsg').find(conditions).toArray();
    
    return branchMsgInfo;
}catch(err) {
    return Boom.notFound("No Message Found");
}
}
const updateBranchMsgByMsgId = async (req) => {
    

    try {
        let newValue = {
            $set: {
                ...req.payload}
        }

    let conditions = {
        _id :ObjectId( req.params.branchMsgId)
    }
    const branchMsgInfo = await req.mongo.db.collection('spbranchMsg').findOneAndUpdate(conditions,newValue);
    
    return branchMsgInfo;
}catch(err) {
    return Boom.notFound("No Message to update");
}
}
const getAllBranchMsg = async (req) => {

    try {

    let conditions = {
       
    }
    const branchMsgInfo = await req.mongo.db.collection('spbranchMsg').find().toArray();
    
    return branchMsgInfo;
}catch(err) {
    return Boom.notFound("No Message Found");
}
}
const addCustomerMsg = async (req) => {

    try {
        

    let conditions = {
        Pincodecovered :req.payload.pincode
    }
    const branchInfo = await req.mongo.db.collection('spbranch').find(conditions).toArray();
    const status = await postMessageToBranch(req,branchInfo);
    return branchInfo;
}catch(err) {
    
    return Boom.notFound("No NearBy Branch Found");
}
}
const postMessageToBranch = async (req,branchInfo) => {
    
        for (branch  of branchInfo) {
        let branchMsg = {
            BranchId: branch.BranchId,
            PinCode: req.payload.pincode,
            Status:"NEW",
            CustomerMobile:req.payload.phoneno,
            createdDate: new Date().toISOString()
        };
        /* let newValue = {
            $set: {
                ...lineUp}
        } */
        const branchMsgInfo = await req.mongo.db.collection('spbranchMsg').insertOne(branchMsg);
        //Write Socket
       // lineUps.push(lineUp);
      }
      return 'Message Posted'
}



module.exports = {
    loginUser,
    getBranchMsgByBranchId,
    getAllBranchMsg,
    updateBranchMsgByMsgId,
    addCustomerMsg
}