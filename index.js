'use strict';

const Hapi = require('@hapi/hapi');
const projectConfig = require('./config/config.json');
const { loginUser,getBranchMsgByBranchId,getAllBranchMsg,updateBranchMsgByMsgId,addCustomerMsg} = require('./lib/routes/route-handler');

const init = async () => {

    const server = Hapi.server({
        port: 7000,
        host: 'localhost'
    })
    var io = require('socket.io')(server.listener);

    await server.register({
        plugin: require('hapi-mongodb'),
        options: {
        //   url: 'mongodb://${conn.user}:${conn.password}@${conn.host}:${conn.port}/${conn.db}',
        url:`mongodb://${projectConfig.mongodb.user}:${projectConfig.mongodb.password}@${projectConfig.mongodb.host}:${projectConfig.mongodb.port}/${projectConfig.mongodb.db}`,
          settings: {
              useUnifiedTopology: true
          },
          decorate: true
        }
    });
    await server.register({
        plugin: require('hapi-cors'),
        options: {
          origins: ['http://localhost:3000'],
          methods: ['POST,PUT, GET,DELETE, OPTIONS'],
          headers: ["Accept", "Content-Type", "sp-access-token","Access-Control-Allow-Headers","ccess-Control-Allow-Origin"]
        }
      });

    server.route({
        method: 'POST',
        path: '/v1/auth/login/',
         handler : async (req,h) => {
          const response = await loginUser(req);
          return response;
        }
      });
      server.route({
        method: 'GET',
        path: '/v1/sp/branch/{branchId}/message/',
         handler : async (req,h) => {
          const response = await getBranchMsgByBranchId(req);
          return response;
        }
      });
      server.route({
        method: 'GET',
        path: '/v1/sp/branch/message/',
         handler : async (req,h) => {
          const response = await getAllBranchMsg(req);
          return response;
        }
      });
      server.route({
        method: 'PUT',
        path: '/v1/sp/branch/message/{branchMsgId}/',
         handler : async (req,h) => {
          const response = await updateBranchMsgByMsgId(req);
          return response;
        }
      });

      server.route({
        method: 'POST',
        path: '/v1/sp/customer/',
         handler : async (req,h) => {
          const response = await addCustomerMsg(req);
          return response;
        }
      });
   server.route({
        method: 'GET',
        path: '/',
        handler: (request, h) => {

            return 'Hello World!';
        }
    }); 

    await server.start();
    console.log('Server running on %s', server.info.uri);
    let interval;
    io.on("connection", (socket) => {
      console.log("New client connected");
     socket.on('FindBranch',function(data) {
       console.log(data);
       if (interval) {
        clearInterval(interval);
      }
      interval = setInterval(() => getApiAndEmit(socket,data), 1000);
      socket.on("disconnect", () => {
        console.log("Client disconnected");
        clearInterval(interval);
      });
     

     })
    });
   /*  const getApiAndEmit = async (socket,data) => {
      const response = new Date();
      // Emitting a new message. Will be consumed by the client
    //  const branchMsgInfo = await server.mongo.db.collection('spbranchMsg').find().toArray();
    // addCustomerMsg(data);
      socket.emit("FromAPI", branchMsgInfo);
    }; */
    const getApiAndEmit = async (socket,data) => {
      console.log("Inside getApiandEmit");
      const branchInfo = await addCustomerData(socket,data);
      console.log(branchInfo);

      //const status = await postMessageToBranch(data,branchInfo,socket);

    }
    const addCustomerData = async (socket,data) => {

      try {
          
  
      let conditions = {
          Pincodecovered :data.pincode
      }
      const branchInfo = await server.mongo.db.collection('spbranch').find(conditions).toArray();
      //const status = await postMessageToBranch(data,branchInfo,socket);
      socket.emit(data.phoneno,branchInfo);
  }catch(err) {
      
     // return Boom.notFound("No NearBy Branch Found");
     socket.emit(data.phoneno,"No NearBy Branch Found");
  }
  }
  const postMessageToBranch = async (data,branchInfo,socket) => {

    console.log("Inside PostMessagetoBranch");
      
          for (branch  of branchInfo) {
          let branchMsg = {
              BranchId: branch.BranchId,
              PinCode: data.pincode,
              Status:"NEW",
              CustomerMobile:data.phoneno,
              createdDate: new Date().toISOString()
          };
          /* let newValue = {
              $set: {
                  ...lineUp}
          } */
          const branchMsgInfo = await server.mongo.db.collection('spbranchMsg').insertOne(branchMsg);
          //Write Socket
          socket.emit(branch.BranchId,branchMsgInfo);
        
        }
        
  }
  
};

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

init();